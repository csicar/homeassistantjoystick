import {
  LitElement,
  html,
  css
} from "https://unpkg.com/lit-element@2.0.1/lit-element.js?module";

class JoystickControl extends LitElement {
  
  static get properties() {
    return {
      hass: {},
      config: {}
    };
  }

  constructor(...args) {
    super(...args)
    this.pos = { x: 0.0, y: 0.0 }
    this.initialOffset = { x: 0.0, y: 0.0 }
    this.currentOffset = { x: 0.0, y: 0.0 }
    this.active = false;
    this.intervalCallback = null;

    this.currentBrightness = null
    this.currentColorTemp = null
  }

  async firstUpdated() {
    this.dragItem = this.shadowRoot.querySelector(".draggable")
  }
  

  render() {
    return html`
      <div class="row" 
        @touchend="${this._dragEnd}" 
        @touchmove="${this._dragMove}" 
        @touchstart="${this._dragStart}"
        @mouseup="${this._dragEnd}" 
        @mousemove="${this._dragMove}" 
        @mousedown="${this._dragStart}"
        
        >
        <div class="draggable"></div>
      </div>
    `;
  }

  setConfig(config) {
    if (!config.entity) {
      throw new Error("You need to define entity");
    }
    this.config = config;
  }

  // The height of your card. Home Assistant uses this to automatically
  // distribute all cards over the available columns.
  getCardSize() {
    return 3;
  }

  _test() {
    console.log(1)
  }

  _dragStart(e) {
    if (e.type === "touchstart") {
      this.initialOffset.x = e.touches[0].clientX - this.currentOffset.x;
      this.initialOffset.y = e.touches[0].clientY - this.currentOffset.y;
    } else {
      this.initialOffset.x = e.clientX - this.currentOffset.x;
      this.initialOffset.y = e.clientY - this.currentOffset.y;
    }
    

    

    if (e.target === this.dragItem) {
      this.active = true;

      if (this.intervalCallback) {
        clearInterval(this.intervalCallback)
      }
  
      const entityState = this.hass.states[this.config.entity]
      this.currentBrightness = entityState.attributes.brightness
      this.currentColorTemp = entityState.attributes.color_temp

      this.intervalCallback = setInterval(() => {
        const entityState = this.hass.states[this.config.entity]
        const distance = Math.sqrt(this.pos.x * this.pos.x + this.pos.y * this.pos.y);
        const angle = Math.atan2(this.pos.y, this.pos.x);
        
        const clampedDistance = this.clamp(distance, 0, 100);
        const clampedX = clampedDistance * Math.cos(angle)
        const clampedY = clampedDistance * Math.sin(angle)
  
        this.currentBrightness = this.clamp(this.currentBrightness - this.scale(clampedY, -200, 200, -40, 40), 1, 255);
        
        this.currentColorTemp = this.clamp(this.currentColorTemp + this.scale(clampedX, -200, 200, -40, 40), 250, 454);
        this._adjust(entityState, this.currentBrightness, this.currentColorTemp);
      }, 200)
    }
  }

  _dragEnd(e) {
    const distance = Math.sqrt(this.pos.x * this.pos.x + this.pos.y * this.pos.y);
    if (distance < 10) {
      // Handle like a click
      this._toggle(this.hass.states[this.config.entity])
    }

    this.initialOffset.x = this.pos.x;
    this.initialOffset.y = this.pos.y;

    this.active = false;
    clearInterval(this.intervalCallback);
    this.intervalCallback = null;
    this.setTranslate(0, 0, this.dragItem);
    this.currentOffset.x = 0
    this.currentOffset.y = 0
    
  }

  _dragMove(e) {
    if (this.active) {
      
      e.preventDefault();
    
      if (e.type === "touchmove") {
        this.pos.x = e.touches[0].clientX - this.initialOffset.x;
        this.pos.y = e.touches[0].clientY - this.initialOffset.y;
      } else {
        this.pos.x = e.clientX - this.initialOffset.x;
        this.pos.y = e.clientY - this.initialOffset.y;
      }

      this.currentOffset.x = this.pos.x;
      this.currentOffset.y = this.pos.y;


      const distance = Math.sqrt(this.pos.x * this.pos.x + this.pos.y * this.pos.y);
      const angle = Math.atan2(this.pos.y, this.pos.x);
      
      const clampedDistance = this.clamp(distance, 0, 150);
      const clampedX = clampedDistance * Math.cos(angle)
      const clampedY = clampedDistance * Math.sin(angle)

      this.setTranslate(clampedX, clampedY, this.dragItem);

    }
  }

  clamp (num, min, max) {
    return Math.max(min, Math.min(max, num));
  }

  scale (num, in_min, in_max, out_min, out_max) {
    return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
  }

 setTranslate(xPos, yPos, el) {
    el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
  }

  _toggle(state) {
    this.hass.callService("light", "toggle", {entity_id: state.entity_id})
  }

  _adjust(state, brightness, color_temp) {
    const cmd = {
      entity_id: state.entity_id,
      brightness,
      color_temp,
      transition: 0.2,
    }
    console.log(cmd)
    this.hass.callService("light", "turn_on", cmd);
  }

    

  static get styles() {
    return css`
      .row {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 100px;
        position: relative;
        /*  from background.svg using https://yoksel.github.io/url-encoder/ */
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='200' width='200'%3E%3Cdefs%3E%3ClinearGradient id='grad1' x1='0%25' y1='0%25' x2='100%25' y2='0%25'%3E%3Cstop offset='0%25' style='stop-color:rgb(0,0,255);stop-opacity:1' /%3E%3Cstop offset='100%25' style='stop-color:orange;stop-opacity:1' /%3E%3C/linearGradient%3E%3ClinearGradient id='grad2' x1='0%25' y1='0%25' x2='0%25' y2='100%25'%3E%3Cstop offset='0%25' style='stop-color:white;stop-opacity:1' /%3E%3Cstop offset='100%25' style='stop-color:rgb(0,0,0);stop-opacity:1' /%3E%3C/linearGradient%3E%3C/defs%3E%3Cellipse opacity='1' cx='100' cy='100' rx='100' ry='100' fill='url(%23grad2)' /%3E%3Cellipse opacity='0.4' cx='100' cy='100' rx='100' ry='100' fill='url(%23grad1)' /%3E%3C/svg%3E");
        background-position: center;
        background-size: contain;
        background-repeat: no-repeat;
      }
      .draggable {
        height: 100px;
        border-radius: 100%;
        width: 100px;
        background: black;
        transition: 0.01s linear transform;
      }
      
    `;
  }
}
customElements.define("joystick-control-card", JoystickControl);
